import React, { Component } from 'react';
import { Container, Header, Content, Tab, Tabs } from 'native-base';
import ListAllArticle from './tabs/ListAllArticle';
import PostArticle from './tabs/PostArticle';
import UpdateArticle from './tabs/UpdateArticle';
import { Provider } from 'react-redux';
import { myStore } from './store/store'

export default class App extends Component {
  render() {
    return (
      <Provider store={myStore}>
        <Container>
        <Header hasTabs />
        <Tabs>
          <Tab heading="Articles">
            <ListAllArticle />
          </Tab>
          <Tab heading="Post">
            <PostArticle />
          </Tab>
          <Tab heading="Update">
            <UpdateArticle />
          </Tab>
        </Tabs>
      </Container>
      </Provider>
    );
  }
}