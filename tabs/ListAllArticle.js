import { Card } from 'native-base'
import React, { Component } from 'react'
import { Text, StyleSheet, View, FlatList, Image, Alert } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { connect } from 'react-redux'
import { fetchArticle , deleteArticle} from '../actions/articleAction'
import Icon from 'react-native-vector-icons/FontAwesome';

class ListAllArticle extends Component {

    componentDidMount() {
        this.props.fetchArticle()
        console.log(this.props.fetchArticle());
    }

    handlerClick = () => {
        Alert.alert('u clicked me!')
    }

    handlerDelete = (id) => {
        // this.props.deleteArticle(1334)
        // var id = this.props.route.params.id
        this.props.deleteArticle(id)
        // Alert.alert(id+'')
        // console.log();
        var dd = id + ''
        console.log(this.props.deleteArticle(dd));
    }

    handlerEdit = () => {
        Alert.alert('handler edit')
    }

    render() {
        return (
            <View>
                <Text style={{ fontSize: 20, color: 'green' }}> List All Article </Text>
                <FlatList
                    data={this.props.articles}
                    renderItem={({ item }) => (
                        <TouchableOpacity
                        >
                            <Card>
                                <View style={{ flexDirection: "row", justifyContent: "space-between", padding: 15 }}>
                                    <View style={{ width: '70%' }}>
                                        <Text numberOfLines={2}
                                            style={{ fontSize: 20, fontWeight: "bold", marginBottom: 12 }}
                                        >{item.title}</Text>
                                        <Text style={{ fontSize: 15, marginBottom: 8 }}>{item.description}</Text>
                                        <TouchableOpacity
                                            onPress = {()=>{this.handlerEdit()}}
                                        >
                                        <Icon
                                            name="edit"
                                            color='blue'
                                            size={25}
                                        />
                                        </TouchableOpacity>

                                        <TouchableOpacity
                                        onPress = {() => this.handlerDelete(item.id)}
                                        >
                                        <Icon
                                            name="trash"
                                            color='red'
                                            size={25}
                                        />
                                        </TouchableOpacity>


                                    </View>
                                    <View style={{ width: '30%' }}>
                                        <Image
                                            source={{ uri: item.image_url }} style={{ width: 120, height: 120, resizeMode: "cover", borderRadius: 10 }}>
                                        </Image>
                                    </View>
                                </View>
                            </Card>

                        </TouchableOpacity>
                    )}
                >

                </FlatList>
            </View>
        )
    }
}

const styles = StyleSheet.create({})

const mapStateToProps = (state) => {
    // console.log(state);
    // console.log('object')
    return {
        articles: state.articles
    }
}

export default connect(mapStateToProps, { fetchArticle , deleteArticle})(ListAllArticle)
