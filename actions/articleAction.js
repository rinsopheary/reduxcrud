export const fetchArticle = () => {
    return (dispatch) => {
        fetch(`http://110.74.194.124:15011/v1/api/articles`, {
            method: 'GET',
            headers: {
                "Authorization": 'Basic QU1TQVBJQURNSU46QU1TQVBJUEBTU1dPUkQ='
            }
        })
            .then(res => {
                return res.json()
            })
            .then(result => {
                console.log(result)
                return dispatch({
                    type: 'get_data',
                    payload: result.data
                })
            }
            )
            .catch(error => {
                console.log(error)
            })
    }
}


export const fectArticle1 = () => {
    return(dispatch)=>{
        fetch(`http://110.74.194.124:15011/v1/api/articles`, {
            method : 'GET',
            headers : {
                "Authorization": "Basic QU1TQVBJQURNSU46QU1TQVBJUEBTU1dPUkQ="
            }
        })
        .then((res)=>{
            return res.json()
        })
        .then((result)=>{
            return dispatch({
                type : 'get_data',
                payload : result.data
            })
        })
        .catch((error)=>{
            
        })
    }
}

export const deleteArticle = (id) => {
    return (dispatch) => {
        fetch(`http://110.74.194.124:15011/v1/api/articles${id}`, {
            method: 'DELETE',
            headers: {
                "Authorization": 'Basic QU1TQVBJQURNSU46QU1TQVBJUEBTU1dPUkQ='
            }
        })
            .then(res => {
                return res.json()
            })
            .then(result => {
                console.log(result)
                return dispatch({
                    type: 'delete_data',
                    payload: result.data
                })
            }
            )
            .catch(error => {
                console.log(error)
            })
    }
}

export const postArticle = (id, title, description, image, author_id, category_id) => {
    var method = id == null ? 'POST' : 'PUT'
    var url = `http://110.74.194.124:15011/v1/api/articles${id == null ? `` : `/${id}`}`
    var type = id == null ? 'post_data' : 'put_data'
    console.log(method, url, type)
    console.log('post article work');
    return (dispatch) => {
        fetch(url, {
            method: method,
            headers: {
                "Authorization": 'Basic QU1TQVBJQURNSU46QU1TQVBJUEBTU1dPUkQ=',
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                "title": title,
                "description": description,
                // "status": "string",
                // "image": "string",
                // "author_id": 0,
                // "category_id": 0
            })
        })
            .then((res) => {
                return res.json()
            })
            .then((result) => {
                console.log('object')
                console.log(result)
                return dispatch({
                    type: type,
                    payload: result.payload
                })
                // console.log('work',result);
            }).catch(error => {
                console.log(error)
            })
    }
}


/*export const postArticle = (title, description,image,author_id,category_id) => {
    console.log('post article work');
    return (dispatch) => {
        fetch(`http://110.74.194.124:15011/v1/api/articles`, {
            method : 'POST',
            headers : {
                "Authorization": 'Basic QU1TQVBJQURNSU46QU1TQVBJUEBTU1dPUkQ=',
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body : JSON.stringify({
                "title": title,
                "description": description,
                // "status": "string",
                // "image": "string",
                // "author_id": 0,
                // "category_id": 0
            })
        })
        .then ((res) => {
            return res.json()
        })
        .then ((result)=>{
            console.log('object')
            console.log(result)
            return dispatch({
                type : 'post_data',
                payload : result.payload
            })
            // console.log('work',result);
        }).catch(error=>{
            console.log(error)
        })
    }
}*/


