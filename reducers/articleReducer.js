
const initState = {
    articles : [],
    article : null
}

export const articleReducer = (state = initState, action) =>{
    switch (action.type) {
        case 'get_data':
            return ({...state, articles : action.payload})
        case 'post_data' : 
            return state
        case 'delete_data' : 
        return { ...state, article: state.articles.filter((a)=>a.id!==action.data.id) }
        default:
            return state;
    }
    // return state
}