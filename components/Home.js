import React, { Component } from 'react'
import { Text, StyleSheet, View } from 'react-native'
import { connect } from 'react-redux'
import {fetchArticle} from '../actions/articleAction'

export class Home extends Component {
    
    componentDidMount(){
        this.props.fetchArticle()
    }
    render() {
        return (
            <View>
                <Text> textInComponent </Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({})

const mapStateToProps = (state) => {
    // console.log(state);
    // console.log('object')
    return {
        articles : state.articles
    }
}

export default connect(mapStateToProps, {fetchArticle}) (Home)
