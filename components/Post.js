import React, { Component } from 'react'
import { Text, StyleSheet, View, TextInput, SafeAreaView, Button } from 'react-native'
import { connect } from 'react-redux'
import { postArticle } from '../actions/articleAction'

export class Post extends Component {

    constructor(props) {
        super(props)

        this.state = {
            id : null,
            title: '',
            description: ''
        }
    }

    componentDidMount() {

        console.log('work')
    }

    handlerPost = () => {
        // alert('hi')

        this.props.postArticle(this.state.id, this.state.title,
            this.state.description)
        // 'https://cdn-images-1.medium.com/max/1024/1*onuKAwAHdvT3zHQkdJodlg.jpeg',
        // 1,
        // 1)
    }
    render() {
        console.log(this.props.articles);
        return (
            <SafeAreaView>
                <Text>ID</Text>
                <TextInput
                    onChangeText={(e) => { this.setState({ id: e }) }}
                ></TextInput>
                <Text>Title</Text>
                <TextInput
                    onChangeText={(e) => { this.setState({ title: e }) }}
                ></TextInput>
                <Text>Description</Text>
                <TextInput
                    onChangeText={(e) => { this.setState({ description: e }) }}
                ></TextInput>
                <Button
                    title='Click'
                    onPress={() => this.handlerPost()}
                />
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({})

const mapStateToProps = (store) => {
    console.log('map state to props work', store);
    return {
        articles: store.articles
    }
}

export default connect(mapStateToProps, { postArticle })(Post)
