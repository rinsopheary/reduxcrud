//1. ActionSheet
/*import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MainPage(),
    );
  }
}

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Cupertino Action sheet demo"),
      ),
      body: Center(
        child: RaisedButton(
          onPressed: () {
            final action = CupertinoActionSheet(
              title: Text("What your favourite food?"),
              message: Text("Select any action ",),
              actions: <Widget>[
                CupertinoActionSheetAction(
                  child: Text("Chicken"),
                  onPressed: () {
                    print("Chicken is clicked");
                  },
                ),
                CupertinoActionSheetAction(
                  child: Text("Burger"),
                  onPressed: () {
                    print("Chicken is clicked");
                  },
                ),
              ],
              cancelButton: CupertinoActionSheetAction(
                child: Text("Cancel"),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            );
            showCupertinoModalPopup(
                context: context, builder: (context) => action);
          },
          child: Text("Click me "),
        ),
      ),
    );
  }
}*/

//2.Button and NavigationBar
/*import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

void main (){
  runApp(Home());
}

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: CupertinoNavigationBar(
          leading: CupertinoNavigationBarBackButton(
            previousPageTitle: 'Home',
            onPressed: () {},
          ),
          middle: const Text('NavigationBar'),
        ),
        body: Container(
          alignment: Alignment.center,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                CupertinoButton(
                  child: Text('Button'),
                  onPressed: () {},
                ),
                CupertinoButton.filled(
                  child: Text('Button'),
                  onPressed: () {},
                ),
              ],
            )
        ),
      ),
    );
  }
}*/
